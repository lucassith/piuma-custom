package core

import (
	"bytes"
	"crypto/sha1"
	"encoding/base64"
	"errors"
	"fmt"
	"image"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"sync"

	"github.com/nfnt/resize"
)

func Optimize(imageData *ImageData, imageParameters ImageParameters, pathtemp string, pathmedia string) (string, string, error) {
	// Get Hash Name
	hash := sha1.New()
	outputType := imageData.ContentType
	if imageParameters.OutputType != KeepMime {
		outputType = imageParameters.OutputType
	}
	hash.Write(
		[]byte(
			fmt.Sprint(
				imageParameters.Width,
				imageParameters.Height,
				imageParameters.Quality,
				outputType,
				imageData.OriginalURL,
				imageData.ContentType,
				imageData.ContentLength,
				imageData.LastModified,
			),
		),
	)
	newFileName := base64.URLEncoding.EncodeToString(hash.Sum(nil))

	newImageTempPath := filepath.Join(pathtemp, newFileName)
	newImageRealPath := filepath.Join(pathmedia, newFileName)

	// Check if file exists
	if _, err := os.Stat(newImageRealPath); err == nil {
		return newImageRealPath, outputType, nil
	}

	// Decode and resize
	reader := bytes.NewReader(imageData.Image)
	var newFileImg *os.File
	var mu = &sync.Mutex{}

	mu.Lock()
	if _, err := os.Stat(newImageTempPath); err == nil {
		return "", "", errors.New("Still elaborating")
	}

	newFileImg, err := os.Create(newImageTempPath)
	mu.Unlock()

	var img image.Image

	imageDecoder, err := NewImageDecoder(imageData.ContentType)
	if err != nil {
		os.Remove(newImageTempPath)
		return "", "", err
	}

	img, err = imageDecoder.Decode(reader)
	if err != nil {
		os.Remove(newImageTempPath)
		return "", "", errors.New("Error decoding response")
	}

	newImage := resize.Resize(imageParameters.Width, imageParameters.Height, img, resize.NearestNeighbor)
	if err != nil {
		os.Remove(newImageTempPath)
		return "", "", errors.New("Error creating new image")
	}

	imageEncoder, err := NewImageEncoder(outputType)
	if err != nil {
		os.Remove(newImageTempPath)
		return "", "", err
	}

	err = imageEncoder.Encode(newFileImg, newImage)
	if err != nil {
		os.Remove(newImageTempPath)
		return "", "", fmt.Errorf("Error encoding response. %w", err)
	}
	newFileImg.Close()

	imageOptimizer, ok := imageEncoder.(ImageOptimizer)
	if ok {
		err = imageOptimizer.Optimize(newImageTempPath, imageParameters.Quality)
		if err != nil {
			os.Remove(newImageTempPath)
			return "", "", err
		}
	}

	err = os.Rename(newImageTempPath, newImageRealPath)
	if err != nil {
		os.Remove(newImageTempPath)
		return "", "", errors.New("Error moving file")
	}

	return newImageRealPath, outputType, nil

}

func BuildResponse(w http.ResponseWriter, imagePath string, contentType string) error {
	img, err := os.Open(imagePath)
	if err != nil {
		return errors.New("Error reading from optimized file")
	}
	defer img.Close()
	w.Header().Set("Content-Type", contentType) // <-- set the content-type header
	io.Copy(w, img)
	return nil
}
