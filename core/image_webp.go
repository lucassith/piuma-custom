package core

import (
	"bytes"
	"errors"
	"image"
	"io"
	"io/ioutil"
	"os"

	"github.com/chai2010/webp"
)

type WEBPHandler struct {
}

func (p *WEBPHandler) ImageType() string {
	return "image/webp"
}

func (p *WEBPHandler) Decode(reader io.Reader) (image.Image, error) {
	return webp.Decode(reader)
}

func (p *WEBPHandler) Encode(newImgFile *os.File, newImage image.Image) error {
	return webp.Encode(newImgFile, newImage, &webp.Options{
		Lossless: true,
	})
}

func (p *WEBPHandler) Optimize(newImageTempPath string, quality uint) error {
	var data []byte
	var err error
	var buf bytes.Buffer

	if quality > 100 {
		return errors.New("Quality must be between 0-100")
	}
	if quality == 100 {
		return nil
	}

	if data, err = ioutil.ReadFile(newImageTempPath); err != nil {
		return err
	}
	loseless := quality > 70

	m, err := webp.Decode(bytes.NewReader(data))
	if err != nil {
		return err
	}

	if err = webp.Encode(&buf, m, &webp.Options{Lossless: loseless, Quality: float32(quality)}); err != nil {
		return err
	}
	if err = ioutil.WriteFile(newImageTempPath, buf.Bytes(), 0666); err != nil {
		return err
	}

	return nil
}
