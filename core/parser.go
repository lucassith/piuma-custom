package core

import (
	"strconv"
	"strings"
)

// This const flags that the output type should be the same as the input type
const KeepMime string = "keep-mime"

// ImageParameters represents the parameters for optimization
type ImageParameters struct {
	Width      uint
	Height     uint
	Quality    uint
	OutputType string
}

// Parser extracts width, height and quality from the provided parameters.
func Parser(name string) (ImageParameters, error) {
	stringSlice := strings.Split(name, "/")
	var dimqual = stringSlice[0]

	paramArray := strings.Split(dimqual, "_")
	arrayOfInt := getDefaultParameters()

	var integerParamLength int
	if len(paramArray) > 3 {
		integerParamLength = 3
	} else {
		integerParamLength = len(paramArray)
	}

	var err error
	var tmpr int

	for i := 0; i < integerParamLength; i++ {
		tmpr, err = strconv.Atoi(paramArray[i])
		if err != nil {
			return ImageParameters{}, err
		}
		arrayOfInt[i] = uint(tmpr)
	}

	outputType := KeepMime

	if len(paramArray) > 3 {
		outputType = parameterToMime(paramArray[3])
	}

	parameters := ImageParameters{
		Width:      arrayOfInt[0],
		Height:     arrayOfInt[1],
		Quality:    arrayOfInt[2],
		OutputType: outputType,
	}
	return parameters, nil
}

// getDefaultParameters creates an the default parameters
// for optimization
func getDefaultParameters() []uint {
	defaultParams := make([]uint, 3)
	defaultParams[0] = 0
	defaultParams[1] = 0
	defaultParams[2] = 100

	return defaultParams
}

func parameterToMime(paramName string) string {
	switch paramName {
	case "webp":
		return "image/webp"
	case "jpg":
		fallthrough
	case "jpeg":
		return "image/jpeg"
	case "png":
		return "image/png"
	default:
		return KeepMime
	}
}
