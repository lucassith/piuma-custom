package core_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "gitlab.com/lucassith/piuma-custom/core"
)

var _ = Describe("Dimensions", func() {
	It("Should scale to exact dimensions", func() {
		currentDimensions := Dimensions{
			A: 100,
			B: 200,
		}
		destinationDimensions := Dimensions{
			A: 1,
			B: 2,
		}
		resized := currentDimensions.ResizeKeepingRatio(destinationDimensions)
		Expect(resized.A).To(Equal(uint(1)))
		Expect(resized.B).To(Equal(uint(2)))
	})

	It("Should upscale to dimension B - 1", func() {
		currentDimensions := Dimensions{
			A: 1,
			B: 2,
		}
		destinationDimensions := Dimensions{
			A: 300,
			B: 6,
		}
		resized := currentDimensions.ResizeKeepingRatio(destinationDimensions)
		Expect(resized.A).To(Equal(uint(3)))
		Expect(resized.B).To(Equal(uint(6)))
	})

	It("Should upscale to dimension B - 2", func() {
		currentDimensions := Dimensions{
			A: 1,
			B: 660,
		}
		destinationDimensions := Dimensions{
			A: 5,
			B: 1600,
		}
		resized := currentDimensions.ResizeKeepingRatio(destinationDimensions)
		Expect(resized.A).To(Equal(uint(2)))
		Expect(resized.B).To(Equal(uint(1600)))
	})

	It("Should downscale to closer dimension A", func() {
		currentDimensions := Dimensions{
			A: 105,
			B: 55,
		}
		destinationDimensions := Dimensions{
			A: 8,
			B: 5,
		}
		resized := currentDimensions.ResizeKeepingRatio(destinationDimensions)
		Expect(resized.A).To(Equal(uint(8)))
		Expect(resized.B).To(Equal(uint(4)))
	})
})
