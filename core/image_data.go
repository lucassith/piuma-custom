package core

import (
	"net/http"
	"strconv"

	"github.com/go-http-utils/headers"
)

type ImageData struct {
	LastModified  string
	ContentType   string
	ContentLength int
	OriginalURL   string
	Image         []byte
}

func ImageDataFromHttpResponse(r *http.Response, imageBody *[]byte, imageURL string) (*ImageData, error) {
	imageData := ImageData{}
	contentLength, _ := strconv.Atoi(r.Header.Get(headers.ContentLength))
	if contentLength == 0 {
		contentLength = len(*imageBody)
	}
	imageData.ContentLength = contentLength
	imageData.ContentType = r.Header.Get(headers.ContentType)
	imageData.LastModified = r.Header.Get(headers.LastModified)
	imageData.OriginalURL = imageURL
	imageData.Image = *imageBody
	return &imageData, nil
}
