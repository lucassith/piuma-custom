package core

import (
	"bytes"
	"image"
	_ "image/jpeg"
	_ "image/png"
	"math"

	"github.com/pkg/errors"
)

type Dimensions struct {
	A, B uint
}

func (d Dimensions) ResizeKeepingRatio(to Dimensions) *Dimensions {
	aRatio := float64(to.A) / float64(d.A)
	bRatio := float64(to.B) / float64(d.B)

	if math.Abs(aRatio) < math.Abs(bRatio) {
		return &Dimensions{
			A: uint(aRatio * float64(d.A)),
			B: uint(aRatio * float64(d.B)),
		}
	}
	return &Dimensions{
		A: uint(bRatio * float64(d.A)),
		B: uint(bRatio * float64(d.B)),
	}
}

func FromImageReader(img []byte) (*Dimensions, error) {
	config, _, err := image.DecodeConfig(bytes.NewReader(img))
	if err != nil {
		return nil, errors.Wrap(err, "Failed to read image file and decode config")
	}
	return &Dimensions{
		A: uint(config.Width),
		B: uint(config.Height),
	}, nil
}
