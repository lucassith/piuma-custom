package core

import (
	"errors"
	"image"
	"io"
	"os"
)

type ImageDecoder interface {
	ImageType() string
	Decode(reader io.Reader) (image.Image, error)
}

type ImageEncoder interface {
	ImageType() string
	Encode(newImgFile *os.File, newImage image.Image) error
}

type ImageOptimizer interface {
	ImageType() string
	Optimize(newImageTempPath string, quality uint) error
}

type ImageHandler interface {
	ImageType() string
	Decode(reader io.Reader) (image.Image, error)
	Encode(newImgFile *os.File, newImage image.Image) error
}

func newImageHandler(imageType string) (ImageHandler, error) {
	switch imageType {
	case "image/jpeg":
		return &JPEGHandler{}, nil
	case "image/png":
		return &PNGHandler{}, nil
	case "image/webp":
		return &WEBPHandler{}, nil
	default:
		return nil, errors.New("Unsupported Image type")
	}
}

func NewImageEncoder(imageType string) (ImageEncoder, error) {
	return newImageHandler(imageType)
}

func NewImageDecoder(imageType string) (ImageDecoder, error) {
	return newImageHandler(imageType)
}
