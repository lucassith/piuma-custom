FROM golang:1.15.5-buster
WORKDIR /app
ENV SOURCE_DIR=/tmp/piuma

RUN apt update && \
 apt install -y ca-certificates pngquant jpegoptim dnsmasq optipng webp gcc-mingw-w64-x86-64 && \
 rm -rf /var/lib/apt/lists/*

RUN mkdir -p ${SOURCE_DIR}
ADD . ${SOURCE_DIR}

RUN cd ${SOURCE_DIR}; go get -u; CGO_ENABLED=1 GOOS=linux go build -o piuma-custom; cp piuma-custom /app && rm -rf /tmp/piuma

WORKDIR /app
ENTRYPOINT ["./piuma-custom"]
