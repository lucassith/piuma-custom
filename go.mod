module gitlab.com/lucassith/piuma-custom

go 1.13

require (
	github.com/bradfitz/gomemcache v0.0.0-20190913173617-a41fca850d0b
	github.com/chai2010/webp v1.1.0
	github.com/go-http-utils/headers v0.0.0-20181008091004-fed159eddc2a
	github.com/golang/protobuf v1.3.3 // indirect
	github.com/gorilla/mux v1.7.4
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/onsi/ginkgo v1.12.0
	github.com/onsi/gomega v1.9.0
	github.com/piumaio/piuma v0.0.0-20200221162008-676118c2587f
	github.com/pkg/errors v0.9.1
	github.com/rainycape/memcache v0.0.0-20150622160815-1031fa0ce2f2
	github.com/stretchr/testify v1.5.1 // indirect
	golang.org/x/net v0.0.0-20200226051749-491c5fce7268 // indirect
	golang.org/x/sys v0.0.0-20200223170610-d5e6a3e2c0ae // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
