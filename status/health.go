package status

type HealthStatus struct {
	Status               string `json:"status"`
	Cache                bool   `json:"cacheStatus",omitempty`
	ProcessMemoryUsage   uint64 `json:"processMemoryUsage"`
	ProcessMemoryUsageMB uint64 `json:"processMemoryUsageMB"`
}
