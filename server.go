package main

import (
	"encoding/base64"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/user"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/bradfitz/gomemcache/memcache"
	"github.com/go-http-utils/headers"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"gitlab.com/lucassith/piuma-custom/core"
	"gitlab.com/lucassith/piuma-custom/exceptions"
	"gitlab.com/lucassith/piuma-custom/mutex"
	"gitlab.com/lucassith/piuma-custom/status"
)

var port string
var pathtemp string
var pathmedia string
var whitelistedDomain string
var disableCache bool
var maximumImageSize int
var sync *mutex.Kmutex
var cache *memcache.Client

const maximumImageSideSize = 3000

func init() {
	log.SetOutput(os.Stdout)
	log.SetFlags(log.Lshortfile | log.Ldate | log.Ltime | log.Lmicroseconds)

	usr, err := user.Current()
	if err != nil {
		log.Printf("[ERROR]: failed getting user [ %s ]\n", err)
		os.Exit(1)
	}

	var memcacheAddress string

	flag.StringVar(&port, "port", "8080", "Port where piuma will run")
	flag.StringVar(&pathmedia, "mediapath", filepath.Join(usr.HomeDir, ".piuma", "media"), "Media path")
	flag.StringVar(&whitelistedDomain, "whitelistedDomain", "", "Whitelisted domain. Only images from provided host will be optimized.")
	flag.BoolVar(&disableCache, "disableCache", false, "Disables image request cache. It will not disable cache for optimized images. Default = false")
	flag.IntVar(&maximumImageSize, "maximumImageSize", 10, "Maximum size of image for optimization")
	flag.StringVar(&memcacheAddress, "memcacheAddress", "localhost:11211", "Address to memcached server")
	flag.Parse()

	if !disableCache {
		memcache := memcache.New(memcacheAddress)
		err = memcache.Ping()
		if err != nil {
			disableCache = true
			log.Printf("Failed to initialise cache. %s\n", err.Error())
		} else {
			cache = memcache
			log.Printf("Connected to memcached at %s\n", memcacheAddress)
		}
	}

	sync = mutex.New()
}

func RatioManager(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	imageParameters, err := core.Parser(params["parameters"])

	if err != nil {
		log.Printf("[ERROR]: parsing parameters [ %s ] : [ %s ]\n", params["parameters"], err)
		w.WriteHeader(400)
		return
	}

	imageData, err := getImageData(params["url"])

	if err != nil {
		handleImageDataError(w, err)
		return
	}

	sourceDimensions, err := core.FromImageReader(imageData.Image)

	if err != nil {
		log.Printf("[ERROR]: error getting image dimensions: [ %s ]\n", err)
		w.WriteHeader(500)
		return
	}

	destinationDimensions := core.Dimensions{
		A: imageParameters.Width,
		B: imageParameters.Height,
	}

	outputDimensions := sourceDimensions.ResizeKeepingRatio(destinationDimensions)

	ratioImageParameters := core.ImageParameters{
		Width:      outputDimensions.A,
		Height:     outputDimensions.B,
		Quality:    imageParameters.Quality,
		OutputType: imageParameters.OutputType,
	}

	optimize(ratioImageParameters, imageData, w)
}

func Manager(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	imageParameters, err := core.Parser(params["parameters"])

	if err != nil {
		log.Printf("[ERROR]: parsing parameters [ %s ] : [ %s ]\n", params["parameters"], err)
	}

	imageData, err := getImageData(params["url"])

	if err != nil {
		handleImageDataError(w, err)
		return
	}

	optimize(imageParameters, imageData, w)
}

func handleImageDataError(w http.ResponseWriter, e error) {
	if _, ok := e.(exceptions.ForbiddenUrl); ok {
		log.Printf("[ERROR]: Provided URL %s is forbidden. Not in whitelisted domain %s.", e, whitelistedDomain)
		w.WriteHeader(403)
		return
	}
	log.Printf("[ERROR]: error fetching image data: [ %s ]\n", e)
	w.WriteHeader(500)
	return
}

func getImageData(url string) (*core.ImageData, error) {
	imageURL, err := getImageURL(strings.TrimLeft(url, "/"))
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("Failed to parse url parameter [%s]\n", url))
	}
	if !isWhitelistedUrl(imageURL) {
		return nil, exceptions.ForbiddenUrl(imageURL)
	}

	imageBody := []byte{}
	var response *http.Response

	sync.Lock(imageURL)

	if !disableCache {
		cacheObject, err := cache.Get(imageURL)
		if err == nil && cacheObject != nil {
			imageBody = cacheObject.Value
			response, err = http.Head(imageURL)
			if err != nil {
				log.Printf("[ERROR] HEAD request failed. Reason: %s\n", err.Error())
				imageBody = []byte{}
				cache.Delete(imageURL)
			} else if response.StatusCode != 200 {
				log.Printf("[ERROR] HEAD request failed. Response code is %d \n", response.StatusCode)
				imageBody = []byte{}
				cache.Delete(imageURL)
			} else if int64(len(imageBody)) != response.ContentLength {
				log.Printf("[INFO] HEAD request ContentLength is %d while cached image is %d", response.ContentLength, len(imageBody))
				imageBody = []byte{}
				cache.Delete(imageURL)
			} else {
				log.Println("[INFO] Loaded file from cache")
			}
		} else {
			log.Printf("[INFO] File [ %s ] wasn't found in the cache.\n", imageURL)
		}
	}

	if len(imageBody) == 0 {
		log.Printf("[INFO] Fetching file from [ %s ].\n", imageURL)
		response, err = http.Get(imageURL)
		if err != nil {
			return nil, errors.Wrap(err, "Failed to get the image body.")
		}
		imageBody, err = ioutil.ReadAll(response.Body)
		response.Body.Close()
		if err != nil {
			return nil, errors.Wrap(err, "Failed to read image body from response.")
		}
		if !disableCache {
			err = cache.Add(
				&memcache.Item{
					Key:        imageURL,
					Expiration: int32((24 * time.Hour).Seconds()),
					Value:      imageBody,
				})
			if err != nil {
				log.Printf("[ERROR] Failed to write file [ %s ] to the cache. Reason: %s\n", imageURL, err.Error())
				cache.Delete(imageURL)
			}
		}
	}

	sync.Unlock(imageURL)

	if err != nil || response.StatusCode != 200 {
		errors.Wrap(err, fmt.Sprintf("failed to fetch image [ %d ] [ %s ]\n", response.StatusCode, err))
	}
	imageData, err := core.ImageDataFromHttpResponse(response, &imageBody, imageURL)
	return imageData, err
}

func optimize(paramaters core.ImageParameters, imageData *core.ImageData, w http.ResponseWriter) {
	imageSizeInMB := float32(imageData.ContentLength) / 1024.0 / 1024.0
	if imageSizeInMB > float32(maximumImageSize) || paramaters.Height > maximumImageSideSize || paramaters.Width > maximumImageSideSize {
		log.Printf(
			"[INFO]: Image under [ %s ] has size of %fMB. Maximum size set to %dMB\n",
			imageData.OriginalURL,
			imageSizeInMB,
			maximumImageSize,
		)
		log.Printf(
			"[INFO]: Image under [ %s ] output dimensions are %dx%d. Maximum side size is %d\n",
			imageData.OriginalURL,
			paramaters.Width,
			paramaters.Height,
			maximumImageSideSize,
		)
		w.Header().Set("Content-Type", imageData.ContentType) // <-- set the content-type header
		w.Header().Set(headers.ContentLength, strconv.Itoa(imageData.ContentLength))
		w.Write(imageData.Image)
		return
	}
	sync.Lock(fmt.Sprintf("OPTIMIZE-%s", imageData.OriginalURL))
	img, contentType, err := core.Optimize(imageData, paramaters, pathtemp, pathmedia)
	sync.Unlock(fmt.Sprintf("OPTIMIZE-%s", imageData.OriginalURL))
	if err != nil {
		log.Printf("[ERROR]: optimizing image [ %s ]\n", err)
		w.Header().Set("Content-Type", imageData.ContentType) // <-- set the content-type header
		w.Header().Set(headers.ContentLength, strconv.Itoa(imageData.ContentLength))
		w.Write(imageData.Image)
		return
	}
	err = core.BuildResponse(w, img, contentType)
}

func isWhitelistedUrl(toTest string) bool {
	u, err := url.Parse(toTest)
	if err != nil {
		return false
	}

	if whitelistedDomain != "" {
		var err error
		host := u.Host
		if strings.Contains(host, ":") {
			host, _, err = net.SplitHostPort(u.Host)
		}
		return err == nil && strings.HasSuffix(host, whitelistedDomain)
	}

	return true
}

func getImageURL(encoded string) (string, error) {
	decoded, err := url.QueryUnescape(encoded)
	if err != nil {
		return "", fmt.Errorf("Failed to unescape query. %v", err)
	}
	if !isValidURL(decoded) {
		base64Decoded, err := base64.StdEncoding.DecodeString(decoded)
		decoded = strings.TrimSpace(string(base64Decoded[:]))
		if err != nil || !isValidURL(decoded) {
			return "", fmt.Errorf("URL parameter is neither valid URL nor base64 string. %v", err)
		}
	}

	return strings.TrimSpace(decoded), nil
}

func isValidURL(toTest string) bool {
	if _, err := url.ParseRequestURI(toTest); err != nil {
		return false
	}

	u, err := url.Parse(toTest)
	if err != nil || u.Scheme == "" || u.Host == "" {
		return false
	}

	return true
}

func Health(w http.ResponseWriter, r *http.Request) {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	health := status.HealthStatus{
		Status:               "OK",
		Cache:                !disableCache,
		ProcessMemoryUsage:   m.Alloc,
		ProcessMemoryUsageMB: m.Alloc / 1024 / 1024,
	}
	output, err := json.Marshal(health)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	w.WriteHeader(200)
	w.Write(output)
}

func main() {
	pathtemp = filepath.Join(pathmedia, "temp")

	os.MkdirAll(pathtemp, os.ModePerm)
	os.MkdirAll(pathmedia, os.ModePerm)

	log.Printf("Running on port %s.\n", port)

	router := mux.NewRouter()
	router.SkipClean(true)
	router.HandleFunc("/_health", Health).Methods("GET")
	router.HandleFunc("/{parameters}/keep-ratio/{url:.*}", RatioManager).Methods("GET")
	router.HandleFunc("/{parameters}/{url:.*}", Manager).Methods("GET")

	log.Fatal(http.ListenAndServe(":"+port, router))
}
