package exceptions

type ForbiddenUrl string

func (e ForbiddenUrl) Error() string {
	return string(e)
}
